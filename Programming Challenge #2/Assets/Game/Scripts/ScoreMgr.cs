﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMgr : Singleton<ScoreMgr>
{
    [SerializeField]
    private int _score;
    public int Score {
        get { return _score; }
        set {
            _score = value;
            textField.text = "Score: " + _score;
        }
    }

    public Text textField;

    // Start is called before the first frame update
    void Start()
    {
        if (textField == null)
        {
            throw new System.Exception("ScoreMgr: text field is not set");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
