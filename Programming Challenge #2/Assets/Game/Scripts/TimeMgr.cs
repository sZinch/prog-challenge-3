﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeMgr : Singleton<TimeMgr>
{
    public Text textField;

    private float timePlayed;

    // Start is called before the first frame update
    void Start()
    {
        if (textField == null)
        {
            throw new System.Exception("TimeMgr: text field is not set");
        }
    }

    // Update is called once per frame
    void Update()
    {
        timePlayed += Time.deltaTime;
        textField.text = "Time: " + Math.Round(timePlayed, 2);
    }
}
