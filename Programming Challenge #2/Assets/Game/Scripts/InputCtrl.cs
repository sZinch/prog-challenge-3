﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InputCtrl : Singleton<InputCtrl>
{
    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    public GameObject upStick;
    public GameObject downStick;
    public GameObject rightStick;
    public GameObject leftStick;

    public float yAxis;
    public float xAxis;

    void Start()
    {
        //Fetch the Raycaster from the GameObject (the Canvas)
        m_Raycaster = GetComponent<GraphicRaycaster>();
        //Fetch the Event System from the Scene
        m_EventSystem = GetComponent<EventSystem>();
    }

    void Update()
    {
        //Check if the left Mouse button is clicked
        if (Input.GetKey(KeyCode.Mouse0) || Input.touches.Length > 0)
        {
            //Set up the new Pointer Event
            m_PointerEventData = new PointerEventData(m_EventSystem);

            if (Input.GetKey(KeyCode.Mouse0))
            {
                m_PointerEventData.position = Input.mousePosition;
            }
            if(Input.touches.Length > 0)
            {
                m_PointerEventData.position = Input.touches[0].position;
            }

            //Create a list of Raycast Results
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            m_Raycaster.Raycast(m_PointerEventData, results);

            //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
            foreach (RaycastResult result in results)
            {
                Debug.Log("Hit " + result.gameObject.name);
            }

            if (results.Count > 0)
            {
                if(results[0].gameObject == upStick)
                {
                    yAxis = 1.0f;
                }
                else if(results[0].gameObject == downStick)
                {
                    yAxis = -1.0f;
                }

                if (results[0].gameObject == rightStick)
                {
                    xAxis = 1.0f;
                }
                else if (results[0].gameObject == leftStick)
                {
                    xAxis = -1.0f;
                }
            }
        }

        else
        {
            yAxis = 0;
            xAxis = 0;
        }
    }
}